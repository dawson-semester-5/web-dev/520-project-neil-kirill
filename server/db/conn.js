require("dotenv").config();
const { MongoClient, ObjectId } = require("mongodb");

const dbUrl = process.env.ATLAS_URI;

let instance = null;

class DAO {
  constructor() {
    if (!instance) {
      instance = this;
      this.client = new MongoClient(dbUrl);
      this.db = null;
      this.collection = null;
    }
    return instance;
  }

  async connect(dbname, collName) {
    if (this.db) {
      return;
    }
    await this.client.connect();
    this.db = this.client.db(dbname);
    console.log("Successfully connected to MongoDB database " + dbname);
    this.collection = this.db.collection(collName);
  }

  async find(query) {
    return await this.collection.find(query).toArray();
  }

  async findById(id) {
    return await this.collection.findOne({ _id: new ObjectId(id) });
  }

  /**
       *
       * @param {Number[]} point1 - longitude and latitude of the first point
       * @param {Number[]} point2 - longitude and latitude of the second point
       * @returns {Promise<WithId<Document>[]>} array of documents in the area given by the box
       *  defined by the two points
       */
  async findByGeoBox([neLon, neLat], [swLon, swLat]) {
    return await this.collection
      .find({
        geo: {
          $geoWithin: {
            $box: [
              [neLon, neLat],
              [swLon, swLat],
            ],
          },
        },
      })
      .toArray();
  }

  /**
       * @param {Number[][]} polygonPoints - array of arrays of longitude and latitude
       * @returns {Promise<WithId<Document>[]>} array of documents in the area given by the polygon
       */
  async findByGeoPolygon(polygonPoints) {
    return await this.collection
      .find({
        geo: {
          $geoWithin: {
            $geometry: {
              type: "Polygon",
              coordinates: [polygonPoints],
            },
          },
        },
      })
      .toArray();
  }

  async findAll(projection) {
    return await this.collection.find().project(projection).toArray();
  }

  async insertMany(array) {
    const result = await this.collection.insertMany(array);
    return result.insertedCount;
  }

  async createIndex(obj) {
    return await this.collection.createIndex(obj);
  }

  async disconnect() {
    if (this.client) {
      this.db = null;
      this.client.close();
      console.log("disconnected");
    }
  }
}

module.exports = DAO;
