// get db connection
const DAO = require("../db/conn");
const parseCSV = require("./load");
const db = new DAO();

async function writetoDb() {
  try {
    await db.connect("WebDevProject", "CitiesGeo");
    await db.insertMany(await parseCSV());
    await db.createIndex({ geo: "2dsphere" });
  } finally {
    if (db) {
      console.log("closed");
      db.disconnect();
    }
  }
}

module.exports = writetoDb;
