const fs = require("fs");
const parse = require("csv-parser");

async function parseCSV(filePath = "./server/utils/cities15000.csv") {
  try {
    return await parseToCities(filePath);
  } catch (err) {
    throw typeof err === "string" ? new Error(err) : err;
  }
}

function parseToCities(filePath) {
  const cities = [];

  return new Promise((resolve, reject) => {
    let count = 0;

    try {
      fs.accessSync(filePath);
    } catch (err) {
      reject(filePath + " not found");
      return;
    }

    const readStream = fs
      .createReadStream(filePath)
      .pipe(parse())
      .on("data", (city) => {
        if (city.asciiname && city.population && city.timezone && !isNaN(city.longitude) && !isNaN(city.latitude)) {
          cities.push({
            name: city.asciiname,
            population: city.population,
            timezone: city.timezone,
            geo: {
              type: "Point",
              coordinates: [parseFloat(city.longitude), parseFloat(city.latitude)],
            },
          });
        }
      })
      .on("end", () => {
        resolve(cities);
      });
  });
}

module.exports = parseCSV;
