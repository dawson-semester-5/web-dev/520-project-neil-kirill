const express = require("express");
const router = express.Router();
const DAO = require("./db/conn");
const db = new DAO();

// cacshing and compression
const compression = require("compression");
router.use(compression());
const cache = require("memory-cache");
router.use((req, res, next) => {
  res.set("Cache-control", "public, max-age=31536000");
  next();
});

// seems like YAML is a less painful route as opposed to jsdocs
const yamlModule = require("js-yaml");
const swaggerUI = require("swagger-ui-express");
const fs = require("fs");
const swaggerDocs = yamlModule.load(fs.readFileSync("./server/description.yaml", "utf-8"));

const swaggerDefinition = {
  openapi: "3.0.0",
  info: {
    title: "Express API for Mapping The Cities",
    version: "1.0.0",
  },
};

// don't have routes yet, future use
const options = {
  swaggerDefinition,
  // Paths to files containing OpenAPI definitions
  apis: ["./routes/*.js"],
};
router.use("/docs", swaggerUI.serve, swaggerUI.setup(swaggerDocs, null, options));

(async () => {
  // connect to the database before starting server
  await db.connect("WebDevProject", "CitiesGeo");

  router.get("/cities/all", async (req, res) => {
    let cities = cache.get("all-cities");
    if (!cities) {
      cities = await db.findAll();
      cache.put("all-cities", cities);
    }
    // const cities = await db.findAll();
    // TODO: add header? to all responses
    res.send(cities);
  });

  router.get("/cities/:id", async (req, res) => {
    const id = req.params.id;
    let cityById = cache.get(id);
    if (!cityById) {
      cityById = await db.findById(id);
      cache.put(id, cityById);
    }
    res.send(cityById);
  });

  // if one is willing to check an area, here is a format:
  // /cities?neLat=45.50884&neLon=-73.58781&swLat=46.50884&swLon=-72.58781
  // note: order for parameters on google: Lat, Lon
  router.get("/cities", async (req, res) => {
    const neLon = parseFloat(req.query.neLon),
      neLat = parseFloat(req.query.neLat),
      swLon = parseFloat(req.query.swLon),
      swLat = parseFloat(req.query.swLat);
    if (isNaN(neLon) || isNaN(neLat) || isNaN(swLon) || isNaN(swLat)) {
      res.status(404).send("Invalid query params");
      res.end();
      return;
    }
    const cacheKey = `${neLat}${neLon}${swLat}${swLon}`;
    let citiesInArea = cache.get(cacheKey);
    if (!citiesInArea) {
      citiesInArea = await db.findByGeoBox([neLon, neLat], [swLon, swLat]);
      cache.put(cacheKey, citiesInArea);
    }
    res.send(citiesInArea);
  });
})();

module.exports = {
  routes: router,
  db: db,
};
