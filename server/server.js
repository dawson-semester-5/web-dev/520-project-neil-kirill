const express = require("express");
const app = express();
const { routes, db } = require("./routes");

const PORT = process.env.PORT || 3001;
let server;

(async () => {
  app.use("/api", routes);

  server = app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
  });

  // disconnect from db on ctrl+c
  process.on("SIGINT", () => {
    disconnect();
    process.exit();
  });
})().catch((err) => {
  console.error(err);
  console.log("disconnecting");
  disconnect();
});

function closeServer() {
  if (!server) {
    return;
  }

  return new Promise((resolve) => {
    server.close((err) => {
      if (err) {
        console.log("Server already closed");
      } else {
        console.log("Server closed");
      }
      resolve();
    });
  });
}

function disconnect() {
  return Promise.all([db.disconnect(), closeServer()]);
}

module.exports = { app, disconnect };
