# 520-Project-Neil-Kirill

Full stack MERN application that creates a visualization of a large dataset.

## Findings and reports

Lighthouse report for desktop: 95

Lighthouse report for mobile: 75
    - In this case most of the throttling was caused by images and the open map itself, issues like string compression and unused js in node_modules

In case of package problems, the best working solution was to manually install the packages to the required folders - deleting package-json does not help
First take took me about 22 different pushes to heroku (unsuccessfull), then I started over and was able to make it work in about 6 pushes, some of them were just some minor syntax.

Markers blink whne using an icon in svg format, that disappears when using png.

Good to know that heroku assumes that the project root is in / and does not require a specific routing other than app.use(express.static(path.join(__dirname, "../client/build"))); in server.js

Deplyment branch is called heroku-v2, this readme is not present there, and also this branch missing some dependencies that heroku requested.