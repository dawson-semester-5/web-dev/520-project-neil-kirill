import L from "leaflet";

const iconPerson = new L.Icon({
  iconUrl: require("../utils/icon.png"),
  iconRetinaUrl: require("../utils/icon.png"),
  iconAnchor: null,
  popupAnchor: null,
  shadowUrl: null,
  shadowSize: null,
  shadowAnchor: null,
  iconSize: new L.Point(60, 60),
  className: "icon",
});

export { iconPerson };
