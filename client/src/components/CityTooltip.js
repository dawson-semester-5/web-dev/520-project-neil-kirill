import React, { Component } from "react";
import { Popup } from "react-leaflet";



class CityTooltip extends Component {
    constructor(props) {
        super(props)
        this.props = props;
    }

    render() {
        const city = this.props.city;
        return (
            <Popup onClose={this.props.onClosed} position={[city.geo.coordinates[1], city.geo.coordinates[0]]}>
                <p>{city.name}, population of {city.population}</p>
                <p>Timezone {city.timezone}</p>
            </Popup >
        )
    }


}

export default CityTooltip;