import React, { Component } from "react";
import { MapContainer, TileLayer, Marker } from "react-leaflet";
import MarkerClusterGroup from "react-leaflet-markercluster";
import "leaflet/dist/leaflet.css";
import "react-leaflet-markercluster/dist/styles.min.css";
import { attribution, tileUrl, startCenter, startZoom, startMapBounds } from "../utils/config";
import CityTooltip from "./CityTooltip";
import { iconPerson } from "./IconComponent";

class MainPopMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arrayOfCities: [],
      selectedCity: {},
    };
    this.map = null;
    this.bounds = startMapBounds;
  }

  async componentDidMount() {
    await this.fetchCities();
  }

  async componentDidUpdate(prevProps) {
    // there is no point in checking whether the props are the same because they are never being used
    await this.fetchCities();
  }

  async fetchCities() {
    let res;

    // this.state.map = null;
    if (this.map !== null) {
      const bounds = this.map.getBounds();
      // if the bounds are the same as the last time, don't fetch again
      if (this.bounds[0][0] === bounds.getNorthEast().lat) {
        return;
      }
      const neLat = bounds.getNorthEast().lat,
        neLon = bounds.getNorthEast().lng,
        swLat = bounds.getSouthWest().lat,
        swLon = bounds.getSouthWest().lng;

      // set the bounds to the new bounds
      this.bounds = [
        [neLat, neLon],
        [swLat, swLon],
      ];

      res = await fetch(`/api/cities?neLat=${neLat}&neLon=${neLon}&swLat=${swLat}&swLon=${swLon}`);
    } else {
      res = await fetch(`/api/cities?neLat=${this.bounds[0][0]}&neLon=${this.bounds[0][1]}&swLat=${this.bounds[1][0]}&swLon=${this.bounds[1][1]}`);
    }

    const cities = await res.json();
    if (JSON.stringify(this.state.arrayOfCities) !== JSON.stringify(cities)) {
      this.setState({ arrayOfCities: cities });
    }
  }

  render() {
    return (
      <MapContainer
        center={startCenter}
        zoom={startZoom}
        minZoom={9}
        scrollWheelZoom={true}
        updateWhenZooming={false}
        updateWhenIdle={true}
        preferCanvas={true}
        whenCreated={(map) => {
          this.map = map;
          map.addEventListener("moveend", () => {
            this.componentDidUpdate();
          });
        }}
      >
        <TileLayer attribution={attribution} url={tileUrl} />
        <MarkerClusterGroup>
          {this.state.arrayOfCities.map((city) => {
            return (
              /* CircleMarkers were very buggy. When the cursor went off of the circle, it remained clickable 
                so we resorted to using Marker instead. */
              <Marker
                key={city._id}
                icon={iconPerson}
                position={[city.geo.coordinates[1], city.geo.coordinates[0]]}
                eventHandlers={{
                  click: () => {
                    this.setState({ selectedCity: city });
                  },
                }}
              />
            );
          })}
        </MarkerClusterGroup>
        {this.state.selectedCity.geo && <CityTooltip onClosed={() => this.setState({ selectedCity: {} })} city={this.state.selectedCity} />}
      </MapContainer>
    );
  }
}

export default MainPopMap;
