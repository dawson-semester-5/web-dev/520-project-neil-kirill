import './App.css';
import React from 'react';
import MainPopMap from "./components/MainPopMap";

function App() {
  return (
    <div className="leaflet-container">
      <MainPopMap />
    </div>
  );
}

export default App;
