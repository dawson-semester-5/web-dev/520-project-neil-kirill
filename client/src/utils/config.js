/* OpenStreetMap */
const attribution = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
const tileUrl = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";

/* Starting Parameters */
const neLat = 45.73;
const neLon = -73.26;
const swLat = 45.27;
const swLon = -74.19;
const startZoom = 11;

const startMapBounds = [
  [neLat, neLon],
  [swLat, swLon],
];
const startCenter = [(neLat + swLat) / 2, (neLon + swLon) / 2];

export { attribution, tileUrl, startCenter, startZoom, startMapBounds };
