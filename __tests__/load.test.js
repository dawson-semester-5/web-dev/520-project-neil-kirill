const { expect } = require("@jest/globals");
const parseCSV = require("server/utils/load");

test("File does not exist", async () => {
    await expect(parseCSV("./no-such-file.csv")).rejects.toThrow();
});

test("File found and parsed with no errors", async () => {
    const cities = await parseCSV();
    expect(cities).not.toBe(undefined);
    for (const obj in cities) {
        expect(cities[obj]).not.toBe(undefined);
    }
});
