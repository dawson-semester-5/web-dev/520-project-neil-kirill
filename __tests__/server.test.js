const request = require("supertest");
const { expect } = require("@jest/globals");
const { app, disconnect } = require("server/server");
const DAO = require("server/db/conn");
jest.mock("server/db/conn");

const parseCSV = require("server/utils/load");

describe("GET /api/cities ", () => {
    afterAll(async () => {
        await disconnect();
    });

    test("gets all cities", async () => {
        const cities = await parseCSV();
        jest.spyOn(DAO.prototype, "findAll").mockResolvedValue(cities);
        const response = await request(app).get("/api/cities/all");
        expect(response.status).toEqual(200);
        expect(Array.isArray(response.body)).toBeTruthy();
        expect(response.body).toEqual(cities);
    });

    test("gets cities in box", async () => {
        const cities = [
            {
                _id: "618e8cafb4330ae92cff1cb9",
                name: "Longueuil",
                population: "229330",
                timezone: "America/Toronto",
                geo: { type: "Point", coordinates: [-73.51806, 45.53121] },
            },
            {
                _id: "618e8cafb4330ae92cff1cc6",
                name: "Montreal",
                population: "1600000",
                timezone: "America/Toronto",
                geo: { type: "Point", coordinates: [-73.58781, 45.50884] },
            },
        ];
        jest.spyOn(DAO.prototype, "findByGeoBox").mockResolvedValue(cities);
        const response = await request(app).get("/api/cities?neLon=-73.58781&neLat=45.50884&swLon=-72.58781&swLat=46.50884");
        expect(response.status).toEqual(200);
        expect(Array.isArray(response.body)).toBeTruthy();
        expect(response.body).toEqual(cities);
    });

    test("get city by mongo id", async () => {
        const city = {
            _id: "618e8cafb4330ae92cff1cc6",
            name: "Montreal",
            population: "1600000",
            timezone: "America/Toronto",
            geo: { type: "Point", coordinates: [-73.58781, 45.50884] },
        };
        jest.spyOn(DAO.prototype, "findById").mockResolvedValue(city);
        const response = await request(app).get("/api/cities/618e8cafb4330ae92cff1cc6");
        expect(response.status).toEqual(200);
        expect(response.body).toEqual(city);
    });

    test("expect 404", async () => {
        const response = await request(app).get("/api/cities");
        expect(response.status).toEqual(404);
        expect(response.text).toEqual("Invalid query params");
    });
});
